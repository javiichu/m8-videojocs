package cat.inspedarlbes.jcp.m8.proves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class ElMeuJocJCP extends ApplicationAdapter {
	SpriteBatch batch;
	
	final  int RESPAWN_ENEMIGOS_EN_SEGUNDOS = 3;
	final  int NAU_SPEED = 150;
	final  int LASER_SPEED = 300;
	
	Rectangle player;
	
	List<Rectangle> enemigos;
	List<Rectangle> disparsPlayer;
	float tiempoTrasUltimoEnemigo = 0; // para almacenar el tiempo desde el ultimo enemigo
	
	Texture playerTexture;
	Texture enemyTexture;
	Texture disparTexture;
	

	
	
	
	// Se ejecuta al principio
	@Override
	public void create () {
		batch = new SpriteBatch();
		
		player = new Rectangle(0,0,70,70);
		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		
		enemigos = new ArrayList<Rectangle>();
		enemyTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
		
		disparsPlayer = new ArrayList<Rectangle>();
		disparTexture = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
		
		// Activem el sistema de Log amg el nivell DEBUG
				Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}
	
	// Se ejecuta continuamente, no tenemos control del cuando
	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		
		//Gestion  de movimiento
		
		gestionInput();
		
		
		//Enemigos
		
		actualizar();
		
		
		//Cada dibujado se ha de hacer entre un begin y un end.
		
		batch.begin();
		
		//Draw tiene muchos constructores, ojo.
		batch.draw(playerTexture, player.x, player.y, player.width, player.height);
		for (Rectangle enemy : enemigos) {
			batch.draw(enemyTexture, enemy.x, enemy.y, enemy.width, enemy.height);
		}
		
		for (Rectangle dispar : disparsPlayer) {
			batch.draw(disparTexture, dispar.x, dispar.y, dispar.width, dispar.height);
		}
		batch.end();
	}

	private void gestionInput() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		
		// EL metode isKeyJustPressed nomes retorna true la primera vegada. Fins que no
		// se solti
		// la tecla i es torni a pressionar no tornara a retornar true
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			// Fem que el dispar surti de la part central de dalt de la nau
			float x = player.x + player.width / 2;
			float y = player.y + player.height; // Recordem que la y es el punt de baix a lesquerra.
			disparsPlayer.add(new Rectangle(x, y, disparTexture.getWidth(), disparTexture.getHeight()));
		}
		
		int amplePantalla = Gdx.graphics.getWidth();
		int altPantalla = Gdx.graphics.getHeight();
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x = player.x - NAU_SPEED * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x = player.x + NAU_SPEED * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			player.y = player.y + NAU_SPEED * delta;
		}
		
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			player.y = player.y - NAU_SPEED * delta;
		}
		

		if (player.x < 0) player.x = 0;
		if (player.x > amplePantalla-player.width) player.x = amplePantalla-player.width;

		if (player.y < 0) player.y = 0;
		if (player.y > altPantalla-player.height) player.y = altPantalla-player.height;
	
	}
	
	private void actualizar() {
		
		int amplePantalla = Gdx.graphics.getWidth();
		int altPantalla = Gdx.graphics.getHeight();
		
		float delta = Gdx.graphics.getDeltaTime();
		
		tiempoTrasUltimoEnemigo += delta;
		
		Random random = new Random();
		
		
		// RESPAWN
		if (tiempoTrasUltimoEnemigo > RESPAWN_ENEMIGOS_EN_SEGUNDOS) {
			int x = random.nextInt((int) (Gdx.graphics.getWidth()-player.width));
			int y = 800;
			
			Rectangle newEnemy = new Rectangle(x,y,70,70);
			enemigos.add(newEnemy);
			
			tiempoTrasUltimoEnemigo = 0;
		}
		
		// MOVIMIENTO LASERS
		
		// Avancem dispars
		for (Rectangle dispar : disparsPlayer) {
			// volem que els dispars vagin cap amunt. Per tant sumem a la y
			dispar.y += LASER_SPEED * delta;
		}
		
		
		
		
		// MOVIMIENTO ENEMIGOS
		
		for (Rectangle enemigo : enemigos) {
			enemigo.y -= NAU_SPEED * delta;
		}
		
		for (Rectangle enemigo : enemigos) {
			if(enemigo.overlaps(player)) Gdx.app.exit();
		}
		
		
		// Mirem si algun dispar esta xocant amb algun enemic
				for (Iterator iterEnemigos = enemigos.iterator(); iterEnemigos.hasNext();) {
					Rectangle enemic = (Rectangle) iterEnemigos.next();

					for (Iterator iterDispar = disparsPlayer.iterator(); iterDispar.hasNext();) {
						Rectangle dispar = (Rectangle) iterDispar.next();

						// Si l'enemic que estem mirant xoca amb el dispar que estem mirant volem
						// eliminar-los als dos
						if (enemic.overlaps(dispar)) {
							iterDispar.remove();
							iterEnemigos.remove();

							// Si no acabo el bucle de dispar, potser torna a accedir a un dispar ja
							// inexistent
							break;
						}
					}
					// Aqui no es pot treballar amb l'enemic ja que l'hem eliminat
				}

		
		// Cuando salen de la pantalla desaparecen
		
		for (Iterator<Rectangle> ite = enemigos.iterator(); ite.hasNext();) {
			Rectangle enemic = (Rectangle) ite.next();
			
			if (enemic.y < -enemic.height) {
				ite.remove();
			}
		}
	}
	
	// Se ejecuta al acabar
	@Override
	public void dispose () {
		batch.dispose();
		playerTexture.dispose();
	}
}
